def calculator(a, b, operator):
    # ==============
    # Your code here
    if operator == "+":
        result = a+b
    elif operator == "-":
        result = a-b
    elif operator == "*":
        result = a*b
    elif operator == "/":
        result = a//b
    else:
        print("Error")
        exit(1)

    binary = ''
    while result > 0:
        binary = binary + (str(result%2))
        result = result//2

    return binary[::-1]
    # ==============

print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
