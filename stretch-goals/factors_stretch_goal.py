def factors(number):
    # ==============
    # Your code here
    factors = []
    for i in range(2,number):
        if int(number % i) == 0:
            factors.append(i)
    if factors == []:
        return "%s is a prime number" %(number)
    else:
        return factors
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
