def vowel_swapper(string):
    # ==============
    # Your code here
    dictionary={'a':'4','e':'3','i':'!','o':'ooo','u':'|_|'}
    for vowel in dictionary:
        lowerString = string.lower()
        try:
            first = lowerString.index(vowel)
            second = lowerString.index(vowel, first+1)
            if string[second] == 'O':
                string = string[0:second]+'000'+string[second+1:]
            else:
                string = string[0:second]+dictionary[vowel]+string[second+1:]
        except ValueError:
            continue
    return string
    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
